const db = require("../data/database");
const jwt = require('jsonwebtoken');

function newCommande(userId, articleId) {
    const date = new Date()
    return new Promise((resolve, rej) => {
        getCommande(userId).then(id => {
            if (id > 0) {
                resolve({ mess: "commande modifié", id: id })
                addToCommande(id, articleId)
            } else {
                db.run("INSERT INTO commande (user_id,date,statut) VALUES(?,?,0)", [userId, date.toLocaleDateString()], function (err, res) {
                    if (err) rej(err)
                    resolve({ mess: "commande créée", id: this.lastID })
                    addToCommande(this.lastID, articleId)
                })
            }
        })
    })
}

function addToCommande(commandeId, articleId) {
    return new Promise((resolve, rej) => {
        db.run("INSERT INTO lignes_commande (commande_id,article_id,quantite) VALUES(?,?,1)", [commandeId, articleId], function (err, res) {
            if (err) rej(err)
            resolve({ mess: "article ajouté au panier", id: this.lastID })
        })
    })
}

function getCommande(userId) {
    return new Promise((resolve, rej) => {
        db.get("SELECT * FROM commande WHERE user_id=? AND statut=0", userId, function (err, res) {
            if (err) rej(err)
            if (res && res.id) resolve(res.id)
            else resolve(0)
        })
    })
}

function validateCommande(userId) {
    return new Promise((resolve, rej) => {
        db.run("UPDATE commande SET statut=1 WHERE user_id=? AND statut=0", userId, function (err, res) {
            if (err) rej(err)
            resolve({ mess: "commande validée", id: this.lastID })
        })
    })
}

function addArticleToCommande(articleId, commandeId) {
    return new Promise((resolve, rej) => {
        db.run("UPDATE lignes_commande SET quantite=quantite+1 WHERE commande_id=? AND article_id=?", [commandeId, articleId], function (err, res) {
            if (err) rej(err)
            resolve({ mess: "article +1" })
        })
    })
}

function removeArticleFromCommande(articleId, commandeId) {
    return new Promise((resolve, rej) => {
        getArticles(articleId, commandeId).then(data => {
            if (data.quantite === 1) {
                resolve({ mess: "article retiré du panier" })
                deleteLignesCommande(data.id)
            } else {
                db.run("UPDATE lignes_commande SET quantite=quantite-1 WHERE commande_id=? AND article_id=?", [commandeId, articleId], function (err, res) {
                    if (err) rej(err)
                    resolve({ mess: "article -1" })
                })
            }
        })
    })
}

function getArticles(articleId, commandeId) {
    return new Promise((resolve, rej) => {
        db.get("SELECT * FROM lignes_commande WHERE commande_id=? AND article_id=?", [commandeId, articleId], function (err, res) {
            if (err) rej(err)
            if (res && res.id) resolve(res.id)
            else resolve(0)
        })
    })
}

function deleteLignesCommande(id) {
    return new Promise((resolve, rej) => {
        db.run("DELETE FROM lignes_commande WHERE id=?", id, (err, res) => {
            if (err) rej(err)
            resolve({ mess: "Article supprimé" })
        })
    })
}

module.exports = {
    newCommande,
    validateCommande,
    removeArticleFromCommande,
    addArticleToCommande
}