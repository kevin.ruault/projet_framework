const express = require('express')
const userRouter = require('./routeur/utilisateur')
const articleRouter = require('./routeur/articles')
const commandeRouter = require('./routeur/commandes')
const db = require('./data/database')
const cors = require('cors')
const app = express()



app.use(express.static("./assets")) //fichiers statiques
app.use(express.json()) //body en json API Rest


app.get("/", (req, res) => {
    res.send("<h1>Bienvenue sur l'API ecommerce</h1>")
})

app.use(cors())
app.use('/user', userRouter)
app.use('/product', articleRouter)
app.use('/commande', commandeRouter)


app.listen(3000)

