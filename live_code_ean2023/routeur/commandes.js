const express = require('express')
const checkToken = require('../middleware/checkToken')
const { newCommande, validateCommande, removeArticleFromCommande, addArticleToCommande } = require('../model/commande')
const routeur = express.Router()

//Add
routeur.post('/', checkToken, (req, res) => {
    newCommande(req.user.userId, articleId).then(data => {
        res.json(data)
    }).catch(err => {
        res.status(500).json(err)
    })
})

routeur.post('/validate', checkToken, (req, res) => {
    validateCommande(req.user.userId).then(data => {
        res.json(data)
    }).catch(err => {
        res.status(500).json(err)
    })
})

routeur.post('/add', checkToken, (req, res) => {
    addArticleToCommande(req.user.userId).then(data => {
        res.json(data)
    }).catch(err => {
        res.status(500).json(err)
    })
})

routeur.post('/remove', checkToken, (req, res) => {
    removeArticleFromCommande(req.user.userId).then(data => {
        res.json(data)
    }).catch(err => {
        res.status(500).json(err)
    })
})

module.exports = routeur