import { useContext } from "react";
import { Link } from "react-router-dom";
import { EcommerceContext } from "../contexte/ecommerce";

export function Entete() {
    const { token } = useContext(EcommerceContext);
    return (
        <header>
            <h1>Mon site</h1>
            <nav>
                <Link to="/">Home</Link>
                {(token !== "") ?
                    <>
                        <Link to="/profile">Profile</Link>
                        <Link to="/logout">Logout</Link>
                    </>
                    :
                    <>
                        <Link to="/login">Connection</Link>
                        <Link to="/register">Inscription</Link>
                    </>
                }

            </nav>
        </header>
    )
}