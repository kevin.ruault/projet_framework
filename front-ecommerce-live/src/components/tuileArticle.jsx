import { useContext } from "react";
import { addToCart } from "../api/commande"
import { EcommerceContext } from "../contexte/ecommerce";

import '../assets/css/tuileArticle.css'


export function TuileArticle( { article } ) {
    const { userId } = useContext(EcommerceContext);
    console.log(article)

    const handleClick = (e) => {
        e.preventDefault()
        addToCart(userId, article.id)
    }

    
    return (
        <div className='tuileArticle-container' key={article.id}>
            <h1>{article.nom}</h1>
            <p>{article.prix}€</p>
            <p>{article.description}</p>
            <button onClick={handleClick}>Ajouter au panier</button>
        </div>
    )
}