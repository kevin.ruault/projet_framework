import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { getProducts } from "../api/product"

import '../assets/css/articles.css'
import { TuileArticle } from "../components/tuileArticle";

export function Articles() {
    const navigate = useNavigate()
    const [productList, setProductList] = useState([])

    useEffect(() => {
        getProducts().then(setProductList)
    }, [])

    return (
        <div className="articles-container">
            <h1>Articles</h1>
            <div className="list">
                {productList.map(e =>
                    <TuileArticle article={e} />
                )}
            </div>
        </div>
    )
}