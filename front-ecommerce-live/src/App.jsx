import './App.css'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { Home } from './pages/home'
import { ErrPage } from './pages/errPage'
import { Entete } from './components/entete'
import { Login } from './pages/login'
import { Inscription } from './pages/inscription'
import { EcommerceContext } from './contexte/ecommerce'
import { useState } from 'react'
import { Profile } from './pages/profile'
import { Logout } from './pages/logout'
import { Articles } from './pages/articles'

function App() {
  const [token, setToken] = useState("");
  const [admin, setAdmin] = useState(false);
  const [userId, setUserId] = useState("")
  return (
    <EcommerceContext.Provider value={{ token, setToken, admin, setAdmin, userId, setUserId }}>
      <BrowserRouter>
        <Entete />
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/login' element={<Login />} />
          <Route path='/register' element={<Inscription />} />
          <Route path='/profile' element={<Profile />} />
          <Route path='/logout' element={<Logout />} />
          <Route path='*' element={<ErrPage />} />
          <Route path='/products' element={<Articles />} />
        </Routes>
      </BrowserRouter>
    </EcommerceContext.Provider>
  )
}

export default App
