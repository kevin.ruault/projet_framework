export function addToCart(article) {
    return new Promise((resolve, reject) => {

        fetch("http://localhost:3000/commande", {
            method: "POST",
            headers: {
                'Content-type': "application/json"
            },
            body: JSON.stringify({ article })
        }).then(res => res.json())
            .then(data => {
                return resolve(data)
            })
            .catch(err => reject(err))
    })
}