export function getProducts() {
    return new Promise((resolve, reject) => {

        fetch("http://localhost:3000/product", {
            method: "GET",
            headers: {
                'Content-type': "application/json"
            },
            body: JSON.stringify()
        }).then(res => res.json())
            .then(data => {
                resolve(data)
            })
            .catch(err => reject(err))
    })
}